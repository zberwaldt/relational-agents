---
name: "Embodied Agents For Long Term Discourse"
title: "Embodied Agents for Long-Term Discourse"
project: null
event: "Phd Dissertation, College of Computer and Information Science, Northeastern University"
authors:
- name: "Schulman, D."
year: 2013
resources:
- name: "schulman_dissertation_final"
  src: "schulman_dissertation_final.pdf"
external_url: null
draft: false 
headless: true
---