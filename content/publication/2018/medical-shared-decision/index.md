---
name: "Medical Shared Decision Making With A"
title: "Medical Shared Decision Making with a Virtual Agent"
project: null
event: "International Conference on Intelligent Virtual Agents (IVA)"
authors:
- name: "Zhang, Z."
- name: "Bickmore, T."
year: 2018
resources: null
external_url: null
draft: false 
headless: true
---