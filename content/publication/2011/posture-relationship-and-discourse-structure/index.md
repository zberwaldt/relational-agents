---
name: "Posture Relationship And Discourse Structure"
title: "Posture, Relationship, and Discourse Structure"
project: null
event: "Intelligent Virtual Agents conference (IVA), Reykjavik, Iceland"
authors:
- name: "Schulman, D."
- name: "Bickmore, T."
year: 2011
resources:
- name: "dan-posture-iva11-FINAL"
  src: "dan-posture-iva11-FINAL.pdf"
external_url: null
draft: false 
headless: true
---